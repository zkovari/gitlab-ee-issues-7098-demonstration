Demonstration for https://gitlab.com/gitlab-org/gitlab-ee/issues/7098

The following queries are not working when *Advanced search functionality* is enabled (tested with Elasticsearch 5.5.2):

 
 * Example 1
   * Query: "us-east-2"
   * Expected result: found "should-be-found"
   * Reality: no results
 * Example 2
   * Query: "us" "east" "2"
   * Expected result: found "should-be-found"
   * Reality: no results
 * Example 3
   * Query: "a;hal;fjhsklfj"
   * Expected result: found "should-be-found-2"
   * Reality: no results
   
   