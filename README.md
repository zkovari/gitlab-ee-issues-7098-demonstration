Demonstration for https://gitlab.com/gitlab-org/gitlab-ee/issues/7098

Try to search (code search) for the following patterns:

 
 * Example 1
   * Query: "us-east-2"
   * Expected result: found "should-be-found"
   * Reality: no results
 * Example 2
   * Query: "us" "east" "2"
   * Expected result: found "should-be-found"
   * Reality: no results
 * Example 3
   * Query: "a;hal;fjhsklfj"
   * Expected result: found "should-be-found-2"
   * Reality: no results
   
   